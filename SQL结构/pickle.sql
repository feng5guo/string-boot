/*
 Navicat Premium Data Transfer

 Source Server         : pickle
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : rm-2zek35174we008e83bo.mysql.rds.aliyuncs.com:3306
 Source Schema         : pickle

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 05/03/2021 00:09:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `nickname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88888889 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `pickle`.`pic`;
CREATE TABLE `pickle`.`pic` (
 `idpic` int(11) NOT NULL auto_increment,
 `caption` varchar(45) NOT NULL default '',
 `img` longblob NOT NULL,
 PRIMARY KEY (`idpic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '88888888', '802311314', '超级用户', '2021-02-25 22:40:38', '2021-02-25 22:40:43');
INSERT INTO `user` VALUES (2, '88888889', '802311315', '超级用户', '2021-02-25 22:40:38', '2021-02-25 22:40:43');

-- ----------------------------
-- Table structure for wx_address
-- ----------------------------
DROP TABLE IF EXISTS `wx_address`;
CREATE TABLE `wx_address`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人',
  `tel` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `sheng` int(0) NOT NULL DEFAULT 0 COMMENT '省id',
  `city` int(0) NOT NULL DEFAULT 0 COMMENT '市id',
  `quyu` int(0) NOT NULL DEFAULT 0 COMMENT '区域id',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货地址（不加省市区）',
  `address_xq` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省市区+详细地址',
  `code` int(0) NOT NULL DEFAULT 0 COMMENT '邮政编码',
  `uid` int(0) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `is_default` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否默认地址 1默认',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_address
-- ----------------------------

-- ----------------------------
-- Table structure for wx_brand
-- ----------------------------
DROP TABLE IF EXISTS `wx_brand`;
CREATE TABLE `wx_brand`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '产品品牌表',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '品牌名称',
  `brandprice` float(8, 2) NOT NULL DEFAULT 0.00 COMMENT '起始价格',
  `photo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `type` tinyint(0) NULL DEFAULT 0 COMMENT '是否推荐',
  `addtime` int(0) NULL DEFAULT NULL COMMENT '添加时间',
  `shop_id` int(0) UNSIGNED NULL DEFAULT 0 COMMENT '商铺id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_brand
-- ----------------------------

-- ----------------------------
-- Table structure for wx_category
-- ----------------------------
DROP TABLE IF EXISTS `wx_category`;
CREATE TABLE `wx_category`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `tid` int(0) NOT NULL DEFAULT 0 COMMENT '父级分类id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '栏目名称',
  `sort` int(0) NOT NULL DEFAULT 0 COMMENT '排序',
  `addtime` int(0) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '栏目简介',
  `bz_1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `bz_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注字段',
  `bz_3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `bz_4` tinyint(0) NOT NULL DEFAULT 0 COMMENT '备用字段',
  `bz_5` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐缩略图',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_category
-- ----------------------------

-- ----------------------------
-- Table structure for wx_china_city
-- ----------------------------
DROP TABLE IF EXISTS `wx_china_city`;
CREATE TABLE `wx_china_city`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键 id',
  `tid` int(0) NULL DEFAULT 0 COMMENT '父级id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `head` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` tinyint(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_china_city
-- ----------------------------

-- ----------------------------
-- Table structure for wx_fankui
-- ----------------------------
DROP TABLE IF EXISTS `wx_fankui`;
CREATE TABLE `wx_fankui`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` int(0) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '反馈内容',
  `addtime` int(0) NOT NULL DEFAULT 0 COMMENT '反馈时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_fankui
-- ----------------------------

-- ----------------------------
-- Table structure for wx_guanggao
-- ----------------------------
DROP TABLE IF EXISTS `wx_guanggao`;
CREATE TABLE `wx_guanggao`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '子页广告管理表',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告名称',
  `photo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `addtime` int(0) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `sort` int(0) NOT NULL DEFAULT 0,
  `type` enum('product','news','partner','index') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'index' COMMENT '广告类型',
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '链接值',
  `position` tinyint(0) UNSIGNED NULL DEFAULT 1 COMMENT '广告位置 1首页轮播',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_guanggao
-- ----------------------------

-- ----------------------------
-- Table structure for wx_order
-- ----------------------------
DROP TABLE IF EXISTS `wx_order`;
CREATE TABLE `wx_order`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单编号',
  `pay_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付单号',
  `shop_id` int(0) NOT NULL DEFAULT 0 COMMENT '商家ID',
  `yonghu_id` int(0) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `price` decimal(9, 2) NOT NULL COMMENT '价格',
  `amount` decimal(9, 2) NULL COMMENT '优惠后价格',
  `addtime` int(0) NOT NULL DEFAULT 0 COMMENT '购买时间',
  `del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '删除状态',
  `type` enum('weixin','alipay','cash') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'weixin' COMMENT '支付方式',
  `price_h` decimal(9, 2) NOT NULL COMMENT '真实支付金额',
  `status` tinyint(0) NOT NULL DEFAULT 10 COMMENT '订单状态{0,已取消 10 未付款  20 待发货  30 待收货  40 待评价  50 交易完成  51 交易关闭}',
  `vid` int(0) NULL DEFAULT 0 COMMENT '优惠券ID',
  `receiver` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人',
  `tel` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `address_xq` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地址详情',
  `code` int(0) NOT NULL COMMENT '邮编',
  `post` int(0) NULL DEFAULT NULL COMMENT '快递ID',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '买家留言',
  `post_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮费信息',
  `product_num` int(0) NOT NULL DEFAULT 1 COMMENT '商品数量',
  `trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信交易单号',
  `kuaidi_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '快递名称',
  `back` enum('1','2','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '标志客户是否有发起退款 1 申请退款  2 已退款',
  `back_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退款原因',
  `back_addtime` int(0) NULL DEFAULT 0 COMMENT '申请退款时间',
  `order_type` tinyint(0) NULL DEFAULT 1 COMMENT '订单类型 1普通订单 2抢购订单',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_order
-- ----------------------------

-- ----------------------------
-- Table structure for wx_order_product
-- ----------------------------
DROP TABLE IF EXISTS `wx_order_product`;
CREATE TABLE `wx_order_product`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '订单商品信息表',
  `pid` int(0) NOT NULL DEFAULT 0 COMMENT '商品id',
  `pay_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付单号',
  `order_id` int(0) NOT NULL DEFAULT 0 COMMENT '订单id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品名称',
  `price` decimal(8, 2) NOT NULL COMMENT '价格',
  `photo_x` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图',
  `pro_buff` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品属性',
  `addtime` int(0) NOT NULL COMMENT '添加时间',
  `num` int(0) NOT NULL DEFAULT 1 COMMENT '购买数量',
  `pro_guige` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格id和规格名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_order_product
-- ----------------------------

-- ----------------------------
-- Table structure for wx_product
-- ----------------------------
DROP TABLE IF EXISTS `wx_product`;
CREATE TABLE `wx_product`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '产品表',
  `shop_id` int(0) NOT NULL DEFAULT 0 COMMENT '商铺id',
  `brand_id` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '品牌ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品名称',
  `intro` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告语',
  `pro_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品编号',
  `price` decimal(8, 2) NOT NULL COMMENT '价格',
  `price_yh` decimal(8, 2) NOT NULL COMMENT '价格',
  `price_jf` int(0) NOT NULL DEFAULT 0 COMMENT '赠送积分',
  `photo_x` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品表',
  `photo_d` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '大图',
  `photo_string` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图片组',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品详情',
  `addtime` int(0) NULL DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(0) NOT NULL COMMENT '修改时间',
  `sort` int(0) NULL DEFAULT 0 COMMENT '排序',
  `renqi` int(0) NOT NULL DEFAULT 0 COMMENT '人气',
  `shiyong` int(0) NOT NULL DEFAULT 0 COMMENT '购买数',
  `num` int(0) NOT NULL DEFAULT 0 COMMENT '数量',
  `type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否推荐  1 推荐    0 不推荐',
  `del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '删除状态',
  `del_time` int(0) NULL DEFAULT 0 COMMENT '删除时间',
  `pro_buff` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品属性',
  `cid` int(0) NOT NULL COMMENT '分类id',
  `company` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `is_show` tinyint(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否是新品',
  `is_down` tinyint(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下架状态',
  `is_hot` tinyint(0) UNSIGNED NULL DEFAULT 0 COMMENT '是否热卖',
  `is_sale` tinyint(1) NULL DEFAULT 0 COMMENT '是否折扣',
  `start_time` int(0) NULL DEFAULT 0 COMMENT '抢购开始时间',
  `end_time` int(0) UNSIGNED NULL DEFAULT 0 COMMENT '抢购结束时间',
  `pro_type` tinyint(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '产品类型   1 普通  2 抢购产品',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_product
-- ----------------------------

-- ----------------------------
-- Table structure for wx_search_record
-- ----------------------------
DROP TABLE IF EXISTS `wx_search_record`;
CREATE TABLE `wx_search_record`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '搜索记录表',
  `uid` int(0) NOT NULL DEFAULT 0 COMMENT '会员ID',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '搜索内容',
  `num` int(0) NOT NULL DEFAULT 1 COMMENT '搜索次数',
  `addtime` int(0) NULL DEFAULT 0 COMMENT '搜索时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_search_record
-- ----------------------------

-- ----------------------------
-- Table structure for wx_shopping_car
-- ----------------------------
DROP TABLE IF EXISTS `wx_shopping_car`;
CREATE TABLE `wx_shopping_car`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '购物车表',
  `pid` int(0) NOT NULL COMMENT '商品ID',
  `price` decimal(9, 2) NOT NULL COMMENT '商品单价',
  `num` int(0) NOT NULL DEFAULT 1 COMMENT '数量',
  `buff` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属性（序列化规格）',
  `addtime` int(0) NOT NULL COMMENT '添加时间',
  `uid` int(0) NOT NULL COMMENT '用户ID',
  `shop_id` int(0) NOT NULL DEFAULT 0 COMMENT '商家ID',
  `gid` int(0) NULL DEFAULT 0 COMMENT '规格id',
  `type` tinyint(0) NULL DEFAULT 2 COMMENT '0是热卖商品，1是团购，2是普通商品',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_shopping_car
-- ----------------------------

-- ----------------------------
-- Table structure for wx_user
-- ----------------------------
DROP TABLE IF EXISTS `wx_user`;
CREATE TABLE `wx_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `openid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小程序用户的openid',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `avatarurl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `gender` tinyint(1) NULL DEFAULT NULL COMMENT '性别  0-男、1-女',
  `country` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所在国家',
  `province` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `language` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语种',
  `ctime` datetime(0) NULL DEFAULT NULL COMMENT '创建/注册时间',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '小程序用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_user
-- ----------------------------

-- ----------------------------
-- Table structure for wx_user_voucher
-- ----------------------------
DROP TABLE IF EXISTS `wx_user_voucher`;
CREATE TABLE `wx_user_voucher`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '会员优惠券领取记录',
  `uid` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `vid` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '优惠券id',
  `shop_id` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商铺ID',
  `full_money` decimal(9, 2) NOT NULL COMMENT '满多少钱',
  `amount` decimal(9, 2) NOT NULL COMMENT '减多少钱',
  `start_time` int(0) NULL DEFAULT 0 COMMENT '开始时间',
  `end_time` int(0) NULL DEFAULT 0 COMMENT '结束时间',
  `addtime` int(0) NOT NULL DEFAULT 0 COMMENT '领取时间',
  `status` tinyint(0) UNSIGNED NULL DEFAULT 1 COMMENT '使用状态 1未使用  2已使用  3已失效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_user_voucher
-- ----------------------------

-- ----------------------------
-- Table structure for wx_voucher
-- ----------------------------
DROP TABLE IF EXISTS `wx_voucher`;
CREATE TABLE `wx_voucher`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '店铺优惠券表',
  `shop_id` int(0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '店铺ID',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优惠券名称',
  `full_money` decimal(9, 2) NOT NULL COMMENT '满多少钱',
  `amount` decimal(9, 2) NOT NULL COMMENT '减多少钱',
  `start_time` int(0) NOT NULL DEFAULT 0 COMMENT '开始时间',
  `end_time` int(0) NOT NULL DEFAULT 0 COMMENT '结束时间',
  `point` int(0) NULL DEFAULT 0 COMMENT '所需积分',
  `count` int(0) UNSIGNED NOT NULL DEFAULT 1 COMMENT '发行数量',
  `receive_num` int(0) UNSIGNED NULL DEFAULT 0 COMMENT '领取数量',
  `addtime` int(0) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '优惠券类型',
  `del` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
  `proid` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品 ID ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voucher
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
