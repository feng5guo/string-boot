package com.example.maket.pickle.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "user")
@Entity
@Data
public class User {
    @Id
    @Column(name = "\"id\"")
    Integer id;

    @Column(name = "\"username\"")
    String username;

    @Column(name = "\"password\"")
    String password;

    @Column(name = "\"nickname\"")
    String nickname;

}
