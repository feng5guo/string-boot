package com.example.maket.pickle.model.dto;

import lombok.Data;

@Data
public class UserDto {
    Integer id;
    String username;
    String password;
    String nickname;
}
