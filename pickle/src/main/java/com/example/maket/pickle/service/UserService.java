package com.example.maket.pickle.service;

import com.example.maket.pickle.model.dto.UserDto;
import com.example.maket.pickle.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository residentInformationRepository;

    public void DeleteUser(UserDto dto) {
        residentInformationRepository.deleteById(String.valueOf(dto.getId()));
    }
}
