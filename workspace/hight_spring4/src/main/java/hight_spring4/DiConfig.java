package hight_spring4;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("hight_spring4")
public class DiConfig {
	
}
