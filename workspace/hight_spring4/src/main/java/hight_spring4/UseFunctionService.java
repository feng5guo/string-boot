package hight_spring4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UseFunctionService {
	@Autowired
	FunctionService functionService;
	
	public String SayHello(String world) {
		return functionService.sayHello(world);
	}
}
