package hight_spring4;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DiConfig.class);
		UseFunctionService usefunctionService = context.getBean(UseFunctionService.class);
		System.out.println(usefunctionService.SayHello("di"));
		context.close();
	}
	
}
